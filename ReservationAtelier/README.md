# Système de Réservation d'Ateliers avec Spring Boot
Cette application Spring Boot fournit un système pour gérer les réservations d'ateliers. Elle inclut des fonctionnalités pour l'authentification des utilisateurs, l'affichage des ateliers, la réalisation de réservations et la gestion des reservations pour un participant.

# Fonctionnalités
## Authentification des utilisateurs
 Authentification personnalisée des utilisateurs avec Spring Security pour gérer les   sessions de connexion et sécuriser les points d'accès.
## Liste des Ateliers 
 Affiche tous les ateliers disponibles et leurs détails.
## Gestion des Réservations 
 Les utilisateurs peuvent effectuer des réservations pour les ateliers, consulter leurs réservations et les annuler.
## Gestion des Erreurs 
 Pages d'erreur personnalisées pour gérer les erreurs spécifiques à l'application de manière élégante.
 
# Configuration et Installation
## Prérequis
Eclipse
JDK 17
Maven 3.1.8
Serveur MySQL 8.0
 
# Configuration de la Base de Données
Créez une base de données nommée workshop_db et mettez à jour src/main/resources/application.properties avec vos identifiants de base de données 

# Lancement de l'Application

## Construisez et lancez l'application avec Maven :
Run as maven clean 
Run as maven install
Run  as Java application
L'application devrait maintenant être en cours d'exécution sur http://localhost:8080.

## Clonez le dépôt (publique) sur le machine locale :
git@gitlab.com:yuni/reservationatelier.git 

# Utilisation
## Naviguez vers http://localhost:8080/ateliers pour voir la liste des ateliers disponibles.
## Pour faire une réservation, l'utilisateur doit d'abord se connecter. Naviguez vers http://localhost:8080/login et entrez vos identifiants.
## Une fois authentifié, vous pouvez réserver un atelier en cliquant sur le bouton de details de la atelier dans la liste de l'atelier, ensuit en cliquant sur le button de  reservevation dans details de l'atelier .
## Vous pouvez voir vos réservations en naviguant vers http://localhost:8080/liste-reservations.
## Pour annuler une réservation, allez à votre liste de réservations et cliquez sur le bouton d'annulation que vous souhaitez annuler.
## Veuillez vous référer à la vidéo suivante pour une démonstration de l'utilisation de l'application:
    /videos/demonstration.mp4


