package com.example.ReservationAtelier.repository;

import com.example.ReservationAtelier.models.Artisan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtisanRepository extends JpaRepository<Artisan, Integer> {

}