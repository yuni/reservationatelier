package com.example.ReservationAtelier.repository;

import com.example.ReservationAtelier.models.Participant;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Integer> {
	Optional<Participant> findByEmail(String email);
}

