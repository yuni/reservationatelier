package com.example.ReservationAtelier.repository;

import com.example.ReservationAtelier.models.Atelier;
import com.example.ReservationAtelier.models.Reservation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
	Optional<Reservation> findById(Integer reservationId);
	int countByAtelier(Atelier atelier);
	List<Reservation> findByParticipantId(Integer participantId);
 
    List<Reservation> findByParticipantIdAndStartTimeLessThanAndEndTimeGreaterThan(Integer participantId, LocalDateTime endTime, LocalDateTime startTime);
}