package com.example.ReservationAtelier.repository;

import com.example.ReservationAtelier.models.Atelier;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtelierRepository extends JpaRepository<Atelier, Integer> {

	 Optional<Atelier> findById(Integer atelierId);
	 List<Atelier> findAll(); 
	 void deleteById(Integer atelierId);
}
