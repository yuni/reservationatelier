package com.example.ReservationAtelier.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomUserDetails implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	private String username;
    private String password;
    private Integer participantId;
    private Collection<? extends GrantedAuthority> authorities;
    
    // Constructeur de CustomUserDetails avec paramètres pour initialiser les propriétés.
    public CustomUserDetails(String username, String password, Integer participantId, Collection<? extends GrantedAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.participantId = participantId;
        this.authorities = authorities;
    }

    // Récupérer les autorités accordées à l'utilisateur.
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    // Récupérer le mot de passe de l'utilisateur.
    @Override
    public String getPassword() {
        return password;
    }

    // Récupérer le nom d'utilisateur.
    @Override
    public String getUsername() {
        return username;
    }

    // Récupérer l'ID du participant associé à l'utilisateur
    public Integer getParticipantId() {
        return participantId;
    }

    // Les méthodes suivantes sont des implémentations des méthodes de UserDetails 
    // pour les détails de compte de l'utilisateur, retournant toutes 'true' pour simplicité.

    // Vérifier si le compte de l'utilisateur n'est pas expiré.
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // Vérifier si le compte de l'utilisateur n'est pas verrouillé.
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // Vérifier si les identifiants de l'utilisateur ne sont pas expirés.
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // Vérifier si l'utilisateur est activé.
    @Override
    public boolean isEnabled() {
        return true;
    }
}
