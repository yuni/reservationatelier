package com.example.ReservationAtelier.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;


public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        Object principal = authentication.getPrincipal();
        String targetUrl = "/home"; // URL de redirection par défaut

        if (principal instanceof CustomUserDetails) {
            CustomUserDetails userDetails = (CustomUserDetails) principal;
            Integer participantId = userDetails.getParticipantId();
            
         // Stocker le participantId dans la session si nécessaire ailleurs
            HttpSession session = request.getSession();
            session.setAttribute("participantId", participantId);

         // Modifier l'URL cible pour inclure le participantId en tant que paramètre de requête
            targetUrl = "/ateliers";
        }

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

     // Utiliser la stratégie de redirection pour rediriger vers l'URL cible modifiée
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
    }

	// Redéfinir la méthode determineTargetUrl pour déterminer l'URL cible de redirection en fonction de l'authentification
	 protected String determineTargetUrl(Authentication authentication) {

	        boolean isAdmin = authentication.getAuthorities().stream()
	                          .anyMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"));

	        if (isAdmin) {
	            return "/admin/dashboard";
	        } else {
	            return "/user/profile";
    	        }
    	    }
}
