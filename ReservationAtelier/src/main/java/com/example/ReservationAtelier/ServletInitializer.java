package com.example.ReservationAtelier;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {
	
	// configurer le servlet initial de l'application lorsque celle-ci est déployée dans un conteneur de servlet traditionnel.
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ReservationAtelierApplication.class);
	}

}
