package com.example.ReservationAtelier.models;

import jakarta.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "artisans")
public class Artisan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_artisan")
    private Integer id;
    
    @Column(name = "prenom",nullable = false, length = 50)
    private String prenom;

    @Column(name = "nom",nullable = false, length = 50)
    private String nom;
    
    @Column(name = "email",nullable = false, unique = true, length = 100)
    private String email;

    @Column(name = "telephone",nullable = false, length = 50)
    private String telephone;
    
    @Column(name = "mot_de_passe", nullable = false, length = 255)
    private String motDePasse;

    @Column(name = "date_inscription")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInscription;

    @OneToMany(mappedBy = "artisan")
    private Set<Atelier> ateliers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getMotDePasse() {
        return motDePasse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Date getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }

    public Set<Atelier> getAtelier() {
        return ateliers;
    }

    public void setAtelier(Set<Atelier> ateliers) {
        this.ateliers = ateliers;
    }
    
    public void addAtelier(Atelier atelier) {
        ateliers.add(atelier);
        atelier.setArtisan(this); // Mise en place d'une relation inverse
    }

    public void removeAtelier(Atelier atelier) {
        ateliers.remove(atelier);
        atelier.setArtisan(null); // Dissoudre la relation inverse
    }
}