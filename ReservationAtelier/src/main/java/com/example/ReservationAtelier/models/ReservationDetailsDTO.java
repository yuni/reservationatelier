package com.example.ReservationAtelier.models;

import java.time.LocalDateTime;

public class ReservationDetailsDTO {
	 // Attributs liées à l'Atelier
    private String videoUrl;
    private String title;
    private Integer maxParticipants;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String location;
    private String description;
    
    // Attributs liées à l'Artisan
    private String artisanNom;
    private String artisanPrenom;
    private String artisanTelephone;
    private String artisanEmail;
    
    // Attributs liées au Participant 
    private Integer participantId;
    private String participantNom;
    private String participantPrenom;
    
    // Attribut liées à l'AtelierReservation 
    private Integer idReservation;
    
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getMaxParticipants() {
		return maxParticipants;
	}
	public void setMaxParticipants(Integer maxParticipants) {
		this.maxParticipants = maxParticipants;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getArtisanNom() {
		return artisanNom;
	}
	public void setArtisanNom(String artisanNom) {
		this.artisanNom = artisanNom;
	}
	public String getArtisanPrenom() {
		return artisanPrenom;
	}
	public void setArtisanPrenom(String artisanPrenom) {
		this.artisanPrenom = artisanPrenom;
	}
	public String getArtisanTelephone() {
		return artisanTelephone;
	}
	public void setArtisanTelephone(String artisanTelephone) {
		this.artisanTelephone = artisanTelephone;
	}
	public String getArtisanEmail() {
		return artisanEmail;
	}
	public void setArtisanEmail(String artisanEmail) {
		this.artisanEmail = artisanEmail;
	}
	public String getParticipantNom() {
		return participantNom;
	}
	public void setParticipantNom(String participantNom) {
		this.participantNom = participantNom;
	}
	public String getParticipantPrenom() {
		return participantPrenom;
	}
	public void setParticipantPrenom(String participantPrenom) {
		this.participantPrenom = participantPrenom;
	}
	public Integer getIdReservation() {
		return idReservation;
	}
	public void setIdReservation(Integer idReservation) {
		this.idReservation = idReservation;
	}
	public Integer getParticipantId() {
		return participantId;
	}
	public void setParticipantId(Integer participantId2) {
		this.participantId = participantId2;
	}

}
