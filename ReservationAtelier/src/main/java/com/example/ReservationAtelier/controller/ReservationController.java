package com.example.ReservationAtelier.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.ui.Model;
import com.example.ReservationAtelier.models.Atelier;
import com.example.ReservationAtelier.models.Participant;
import com.example.ReservationAtelier.models.ReservationDetailsDTO;
import com.example.ReservationAtelier.service.AtelierService;
import com.example.ReservationAtelier.service.ParticipantService;
import com.example.ReservationAtelier.service.ReservationService;
import com.example.ReservationAtelier.service.ReservationServiceImpl.AtelierFullException;
import com.example.ReservationAtelier.service.ReservationServiceImpl.ReservationConflictException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;


@Controller
public class ReservationController {

    @Autowired
    private AtelierService atelierService; // gérer la logique commerciale liée à l'Atelier

    @Autowired
    private ReservationService reservationService; // gérer la logique commerciale liée à la réservation

    @Autowired
    private ParticipantService participantService; // gérer la logique commerciale liée au participant

 // Méthode pour effectuer la réservation d'un atelier
    @PostMapping("/reserveAtelier")
    public String reserveAtelier(@RequestParam("atelierId") Integer atelierId, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        Optional<Participant> participantOpt = participantService.findByEmail(username);
        if (!participantOpt.isPresent()) {
        	redirectAttributes.addFlashAttribute("errorMessage", "Participant non trouvé");
            return "redirect:/error";
        }

        Optional<Atelier> atelierOpt = atelierService.findById(atelierId);
        if (!atelierOpt.isPresent()) {
        	redirectAttributes.addFlashAttribute("errorMessage", "Atelier non trouvé");
            return "redirect:/error";
        }

        try {
            reservationService.createReservation(atelierId, participantOpt.get().getId(), "Confirmed");
            redirectAttributes.addAttribute("atelierId", atelierId);
            redirectAttributes.addAttribute("participantId", participantOpt.get().getId());
            return "redirect:/liste-reservations";
        } catch (EntityNotFoundException | ReservationConflictException | AtelierFullException e) {
        	redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return "redirect:/error";
        }
    }

    // Méthode pour afficher la page d'erreur
	@GetMapping("/error")
	public String errorPage(RedirectAttributes redirectAttributes, Model model) {
	    model.addAttribute("errorMessage", "This is a test error message.");
	    return "error";
	}
	
	// Méthode pour afficher la liste des réservations d'un participant.
    @GetMapping("/liste-reservations")
    public String showReservations(Model model, @RequestParam("participantId") Integer participantId) {
        List<ReservationDetailsDTO> reservationDetails = reservationService.getAllReservationDetailsForParticipant(participantId);
        model.addAttribute("reservationDetails", reservationDetails);
        return "liste-reservations";
    }

    
     // Méthode pour supprimer une réservation.
    @GetMapping("/deleteReservation/{id}")
    public String deleteReservation(@PathVariable("id") Integer reservationId, RedirectAttributes redirectAttributes) {
        try {

            Integer participantId = reservationService.findParticipantIdByReservationId(reservationId);
            
            reservationService.deleteReservation(reservationId);
            
            redirectAttributes.addFlashAttribute("successMessage", "La réservation a été supprimée avec succès.");

            return "redirect:/liste-reservations?participantId=" + participantId;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Échec de la suppression de la réservation.");
            return "redirect:/liste-reservations";
        }
    }    
}
