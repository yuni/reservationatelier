package com.example.ReservationAtelier.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController {

    public String handleError(HttpServletRequest request, Model model) {
        // Obtenir un code d'état d'erreur à partir d'une HttpServletRequest
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            // Définition de noms de vues ou d'attributs de modèles différents en fonction des codes d'état
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error-404";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error-500";
            }
        }
        return "error"; // Vue d'erreur par défaut
    }
}

