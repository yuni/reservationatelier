package com.example.ReservationAtelier.controller;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.example.ReservationAtelier.models.Participant;
import com.example.ReservationAtelier.service.ParticipantService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;


@Controller
public class ParticipantController {

	@Autowired
    private PasswordEncoder passwordEncoder;
	
    @Autowired
    private ParticipantService participantService;

    @GetMapping("/register")
    public String showRegistrationForm(Model model) {
        model.addAttribute("participant", new Participant());
        return "register";
    }

    @PostMapping("/register")
    public String registerParticipant(@ModelAttribute Participant participant, BindingResult result,
        @RequestParam("password_confirmation") String passwordConfirm,

        RedirectAttributes attributes) {
		// Confirmer que les mots de passe saisis deux fois sont les mêmes
		if (!participant.getMotDePasse().equals(passwordConfirm)) {
		attributes.addFlashAttribute("error", "Passwords do not match!");
		return "redirect:/register";
		}
		
		 // Fixer la date d'enregistrement à la date du jour
	    participant.setDateInscription(new Date());
	    
		// Cryptage des mots de passe et sauvegarde des utilisateurs
		participant.setMotDePasse(passwordEncoder.encode(participant.getMotDePasse()));
		participantService.saveParticipant(participant);
		
		// Redirection vers la page de connexion ou d'autres pages
		 attributes.addFlashAttribute("success", "Registration successful. You can now login.");
		return "redirect:/login";
	}
    
	    @GetMapping("/login")
	    public String showLoginForm() {
	    
	        return "login";
	    }
	    
	    
}