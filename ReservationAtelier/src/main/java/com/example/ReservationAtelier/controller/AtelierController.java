package com.example.ReservationAtelier.controller;

import com.example.ReservationAtelier.models.Atelier;
import com.example.ReservationAtelier.service.AtelierService;
import java.util.List;
//Importation pour l'injection de dépendance
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
//Importation pour marquer la classe comme contrôleur
import org.springframework.stereotype.Controller; 
//Importation pour passer des données à la vue
import org.springframework.ui.Model;
//Importation pour gérer les requêtes GET
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;


@Controller 
public class AtelierController {

  // Injection du service AtelierService grâce à l'annotation @Autowired.
  private final AtelierService atelierService; 

  @Autowired 
  public AtelierController(AtelierService atelierService) {
      this.atelierService = atelierService; 
  }

  //Gère les requêtes GET pour "/ateliers" et retourne la vue avec la liste des ateliers.
  @GetMapping("/ateliers")
  public String showAllAteliers(Model model) {
      List<Atelier> ateliers = atelierService.getAllAteliers();
      model.addAttribute("ateliers", ateliers);    // Ajoute la liste des ateliers au modèle pour la vue.
      return "liste-ateliers"; // Retourne le nom de la vue à afficher
  }

  //Gère les requêtes GET pour "/atelier/{id}" et retourne la vue avec les détails d'un atelier spécifique
  @GetMapping("/atelier/{id}")
  public String showAtelierDetails(@PathVariable("id") Integer id, Model model) {
	  // Recherche un atelier par son ID, sinon lève une exception si non trouvé.
	  Atelier atelier = atelierService.findById(id)
			    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Atelier not found"));
      model.addAttribute("atelier", atelier);// Ajoute l'atelier trouvé au modèle pour la vue
      return "details-atelier";// Retourne le nom de la vue à afficher
  }

  //Gère les requêtes GET pour "/videos" et retourne la vue avec la liste des URLs de vidéos
  @GetMapping("/videos")
  public String showVideoList(Model model) {
      List<String> videoUrls = atelierService.getAllVideoUrls();
      model.addAttribute("videoUrls", videoUrls);// Ajoute la liste des URLs de vidéos au modèle pour la vue
      return "/home";  // Retourne le nom de la vue à afficher
  }
}

