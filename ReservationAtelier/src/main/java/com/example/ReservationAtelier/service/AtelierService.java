package com.example.ReservationAtelier.service;

import com.example.ReservationAtelier.models.Atelier;
import java.util.List;
import java.util.Optional;


public interface AtelierService {
    Atelier saveAtelier(Atelier atelier);
    List<Atelier> getAllAteliers();
    Atelier updateAtelier(Atelier atelier);
    void deleteAtelier(Integer id);
    Optional<Atelier> findById(Integer atelierId);
	List<String> getAllVideoUrls();
}
