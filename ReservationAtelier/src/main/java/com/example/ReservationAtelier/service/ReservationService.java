package com.example.ReservationAtelier.service;

import com.example.ReservationAtelier.models.Reservation;
import com.example.ReservationAtelier.models.ReservationDetailsDTO;
import com.example.ReservationAtelier.service.ReservationServiceImpl.AtelierFullException;
import com.example.ReservationAtelier.service.ReservationServiceImpl.ReservationConflictException;

import jakarta.persistence.EntityNotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


public interface ReservationService {
	Reservation saveReservation(Reservation reservation);
	Optional<Reservation> getReservationById(Integer id);
    Reservation updateReservation(Reservation reservation);
    void deleteReservation(Integer id);
    int getParticipantsCount(Integer atelierId);
	boolean isReservationConflict(Integer atelierId, LocalDateTime startTime, LocalDateTime endTime);
	List<ReservationDetailsDTO> getAllReservationDetailsForParticipant(Integer participantId);
	Reservation createReservation(Integer atelierId, Integer participantId, String status)
			throws EntityNotFoundException, ReservationConflictException, AtelierFullException;
	Integer findParticipantIdByReservationId(Integer reservationId);	
}
