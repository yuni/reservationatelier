package com.example.ReservationAtelier.service;

import  com.example.ReservationAtelier.models.Atelier;
import  com.example.ReservationAtelier.repository.AtelierRepository;
import jakarta.transaction.Transactional; // Importation pour la gestion des transactions.
import org.springframework.beans.factory.annotation.Autowired; // Importation pour l'injection de dépendance.
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service; // Importation pour indiquer que c'est un service Spring.

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service // Annoter la classe comme service pour que Spring gère sa création et son cycle de vie.
@Transactional 
public class AtelierServiceImpl implements AtelierService {

	@Autowired
	private AtelierRepository atelierRepository;

	@Autowired
	private ReservationService reservationService;
	
	@Autowired // Utilisation de l'injection de champs et ajout de l'annotation @Lazy
    public void setReservationService(@Lazy ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Override // Implémentation de la méthode pour obtenir tous les ateliers.
    public List<Atelier> getAllAteliers() {
        return atelierRepository.findAll(); // Appel du repository pour obtenir tous les ateliers.
    }

    @Override // Implémentation de la méthode pour mettre à jour un atelier.
    public Atelier updateAtelier(Atelier atelier) {
        return atelierRepository.save(atelier); // Appel du repository pour enregistrer (et donc mettre à jour) l'atelier.
    }

    @Override // Implémentation de la méthode pour supprimer un atelier par son ID.
    public void deleteAtelier(Integer id) {
    	atelierRepository.deleteById(id); // Appel du repository pour supprimer l'atelier.
    }
   
    @Override
    public Atelier saveAtelier(Atelier atelier) {
        return atelierRepository.save(atelier);
    }


	@Override
	public Optional<Atelier> findById(Integer atelierId) {
		return atelierRepository.findById(atelierId);
				
	}
	
	 @Override
	    public List<String> getAllVideoUrls() {
	        List<Atelier> ateliers = atelierRepository.findAll();
	        List<String> videoUrls = new ArrayList<>();
	        for (Atelier atelier : ateliers) {
	            String videoUrl = atelier.getVideoUrl();
	            if (videoUrl != null) {
	                videoUrls.add(videoUrl);
	            }
	        }
	        return videoUrls;
	    }
}
