package com.example.ReservationAtelier.service;

import com.example.ReservationAtelier.models.Artisan;
import com.example.ReservationAtelier.models.Atelier;
import com.example.ReservationAtelier.models.Participant;
import  com.example.ReservationAtelier.models.Reservation;
import com.example.ReservationAtelier.models.ReservationDetailsDTO;
import com.example.ReservationAtelier.repository.AtelierRepository;
import com.example.ReservationAtelier.repository.ParticipantRepository;
import  com.example.ReservationAtelier.repository.ReservationRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional; // Importation pour la gestion des transactions.
import org.springframework.beans.factory.annotation.Autowired; // Importation pour l'injection de dépendance.
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service; // Importation pour indiquer que c'est un service Spring.
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service // Annoter la classe comme service pour que Spring gère sa création et son cycle de vie.
@Transactional // Annoter la classe pour indiquer que toutes les méthodes publiques sont exécutées
//dans le cadre d'une transaction.
public class ReservationServiceImpl implements ReservationService {

		@Autowired
	    private ReservationRepository reservationRepository;

		@Autowired
	    private ParticipantRepository participantRepository;
 
	    @Autowired
	    private AtelierRepository atelierRepository;

	    @Autowired
	    private ArtisanService artisanService;

	    @Autowired
	    private ParticipantService participantService;
	    
	    // Utilisation de l'annotation @Lazy
	    @Lazy
	    @Autowired
	    private AtelierService atelierService; // Utilisation de l'annotation @Lazy
    
	    @Override
	    public Reservation createReservation(Integer atelierId, Integer participantId, String status) throws EntityNotFoundException, ReservationConflictException, AtelierFullException {
	    	try {
	    		// Recherche l'atelier par son identifiant. Lance une exception si non trouvé.
	            Atelier atelier = atelierRepository.findById(atelierId)
	                .orElseThrow(() -> new EntityNotFoundException("Atelier not found with id: " + atelierId));
	            
	            // Recherche le participant par son identifiant. Lance une exception si non trouvé.
	            Participant participant = participantRepository.findById(participantId)
	                .orElseThrow(() -> new EntityNotFoundException("Participant not found with id: " + participantId));
	            
	            // Vérifie s'il y a un conflit de réservation pour le participant aux dates et heures de l'atelier.
	            boolean isConflict = isReservationConflict(participant.getId(), atelier.getStartTime(), atelier.getEndTime());
	            if (isConflict) {	               
	                throw new ReservationConflictException("Reservation conflict ! Vous avez déjà une réservation en même temps.");
	            }
	            
	            // Compte le nombre de participants déjà inscrits à l'atelier
	            int participantsCount = getParticipantsCount(atelierId);
	            // Vérifie si l'atelier a atteint sa capacité maximale de participants
	            if (participantsCount >= atelier.getMaxParticipants()) {
	                throw new AtelierFullException("L'atelier est complet ! Il n'y a plus de réservations !");
	            }

	            // Crée une nouvelle réservation
	            Reservation reservation = new Reservation();
	            reservation.setAtelier(atelier);
	            reservation.setParticipant(participant);
	            reservation.setDateCreation(LocalDateTime.now());
	            reservation.setStatus(status);
	            reservation.setStartTime(atelier.getStartTime()); 
	            reservation.setEndTime(atelier.getEndTime()); 
	            
	            // Enregistre la réservation dans la base de données
	            Reservation savedReservation = reservationRepository.save(reservation);
	            return savedReservation;
	        } catch (EntityNotFoundException | ReservationConflictException | AtelierFullException e) {
	            throw e;
	        } catch (Exception e) {
	            throw new RuntimeException("Unexpected error during reservation creation", e);
	        }
	    }
	  


	    public class ReservationConflictException extends RuntimeException {
			private static final long serialVersionUID = 1L;
			public ReservationConflictException(String message) {
	            super(message);
	        }
	    }

	    public class AtelierFullException extends RuntimeException {
			private static final long serialVersionUID = 1L;
			public AtelierFullException(String message) {
	            super(message);
	        }
	    }

	    @Override
	    public boolean isReservationConflict(Integer participantId, LocalDateTime startTime, LocalDateTime endTime) {
	        List<Reservation> conflictingReservations = reservationRepository.findByParticipantIdAndStartTimeLessThanAndEndTimeGreaterThan(participantId, endTime, startTime);
	        return !conflictingReservations.isEmpty();
	    }

   
	    @Override
	    public int getParticipantsCount(Integer atelierId) {
	        Atelier atelier = atelierRepository.findById(atelierId)
	            .orElseThrow(() -> new EntityNotFoundException("Atelier not found with id: " + atelierId));
	        return reservationRepository.countByAtelier(atelier);
	    }
    
    @Override // Implémentation de la méthode pour enregistrer une reservation.
    public Reservation saveReservation(Reservation reservation) {
    	System.out.println("Saving reservation: " + reservation);
        return reservationRepository.save(reservation); // Appel du repository pour enregistrer la reservation.
    }

    @Override // Implémentation de la méthode pour obtenir une reservation par son ID.
    public Optional<Reservation> getReservationById(Integer id) {
        return reservationRepository.findById(id); // Appel du repository pour trouver la reservation.
    }

    @Override
    public List<ReservationDetailsDTO> getAllReservationDetailsForParticipant(Integer participantId) {
        List<Reservation> reservations = reservationRepository.findByParticipantId(participantId);
        List<ReservationDetailsDTO> detailsList = new ArrayList<>();

        for (Reservation reservation : reservations) {
        	Integer artisanId = null;
            Atelier atelier = atelierService.findById(reservation.getAtelier().getId()).orElse(null);
            if (atelier.getArtisan() != null) {
               artisanId = atelier.getArtisan().getId();              
            }
                       
			Artisan artisan = artisanService.getArtisanById(artisanId).orElse(null);
            Participant participant = participantService.getParticipantById(participantId).orElse(null);
         
            
            ReservationDetailsDTO dto = new ReservationDetailsDTO();
            dto.setIdReservation(reservation.getId()); 
            if (atelier != null) {
                dto.setVideoUrl(atelier.getVideoUrl());
                dto.setTitle(atelier.getTitle());
                dto.setMaxParticipants(atelier.getMaxParticipants());
                dto.setStartTime(atelier.getStartTime());
                dto.setEndTime(atelier.getEndTime());
                dto.setLocation(atelier.getLocation());
                dto.setDescription(atelier.getDescription());
            }
            if (artisan != null) {
                dto.setArtisanNom(artisan.getNom());
                dto.setArtisanPrenom(artisan.getPrenom());
                dto.setArtisanTelephone(artisan.getTelephone());
                dto.setArtisanEmail(artisan.getEmail());
            }
            if (participant != null) {
            	dto.setParticipantId(participantId);
                dto.setParticipantNom(participant.getNom());
                dto.setParticipantPrenom(participant.getPrenom());
            }

            detailsList.add(dto);
        }
        return detailsList;
    }



    @Override // Implémentation de la méthode pour mettre à jour une reservation.
    public Reservation updateReservation(Reservation reservation) {
        return reservationRepository.save(reservation); // Appel du repository pour enregistrer (et donc mettre à jour) la reservation.
    }

    @Override // Implémentation de la méthode pour supprimer une reservation par son ID.
	 public void deleteReservation(Integer idReservation) {
	        // 删除数据库中的预定记录
	        reservationRepository.deleteById(idReservation);
	    }


    @Override
    public Integer findParticipantIdByReservationId(Integer reservationId) {
        return reservationRepository.findById(reservationId)
                .map(Reservation::getParticipant)
                .map(Participant::getId)
                .orElse(null); 
    }
}



