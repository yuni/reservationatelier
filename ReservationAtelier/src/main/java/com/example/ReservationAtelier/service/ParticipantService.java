package com.example.ReservationAtelier.service;

import com.example.ReservationAtelier.models.Participant;
import java.util.List;
import java.util.Optional;

public interface ParticipantService {
	Participant saveParticipant(Participant participant);
    List<Participant> getAllParticipants();
    Participant updateParticipant(Participant atelier);
    String getNomById(Integer id);
	String getPrenomById(Integer id);	
    void deleteParticipant(Integer id);
    Optional<Participant> getParticipantById(Integer id);
	 Optional<Participant> findByEmail(String email);   
}
