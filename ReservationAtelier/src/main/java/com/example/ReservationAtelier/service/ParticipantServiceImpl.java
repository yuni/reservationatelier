package com.example.ReservationAtelier.service;

import  com.example.ReservationAtelier.models.Participant;
import  com.example.ReservationAtelier.repository.ParticipantRepository;
import jakarta.transaction.Transactional; // Importation pour la gestion des transactions.

import org.springframework.beans.factory.annotation.Autowired; // Importation pour l'injection de dépendance.
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service; // Importation pour indiquer que c'est un service Spring.
import java.util.List;
import java.util.Optional;

@Service // Annoter la classe comme service pour que Spring gère sa création et son cycle de vie.
@Transactional // Annoter la classe pour indiquer que toutes les méthodes publiques sont exécutées
//dans le cadre d'une transaction.
public class ParticipantServiceImpl implements ParticipantService {

    private final ParticipantRepository participantRepository; // Déclaration du repository pour les participants.
    
    @Autowired
    public ParticipantServiceImpl(ParticipantRepository participantRepository, PasswordEncoder passwordEncoder) {
        this.participantRepository = participantRepository;
    }

    
    @Override // Implémentation de la méthode pour enregistrer un participant.
    public Participant saveParticipant(Participant participant) {
        return participantRepository.save(participant); // Appel du repository pour enregistrer le participant.
    }

    @Override // Implémentation de la méthode pour obtenir un participant par son ID.
    public Optional<Participant> getParticipantById(Integer id) {
        return participantRepository.findById(id);
         
    }

    @Override // Implémentation de la méthode pour obtenir tous les participants.
    public List<Participant> getAllParticipants() {
        return participantRepository.findAll(); // Appel du repository pour obtenir tous les participants.
    }

    @Override // Implémentation de la méthode pour mettre à jour un participant.
    public Participant updateParticipant(Participant participant) {
        return participantRepository.save(participant); // Appel du repository pour enregistrer (et donc mettre à jour) le participant.
    }

    @Override // Implémentation de la méthode pour supprimer un participant par son ID.
    public void deleteParticipant(Integer id) {
    	participantRepository.deleteById(id); // Appel du repository pour supprimer le participant.
    }

    @Override
    public String getNomById(Integer id) {
        Optional<Participant> participantOptional = participantRepository.findById(id);
        if (participantOptional.isPresent()) {
            return participantOptional.get().getNom();
        }
        return "";
    }

    @Override
    public String getPrenomById(Integer id) {
        Optional<Participant> participantOptional = participantRepository.findById(id);
        if (participantOptional.isPresent()) {
            return participantOptional.get().getPrenom();
        }
        return "";
    }


	@Override
	public Optional<Participant> findByEmail(String email) {
		// Renvoie un Optional contenant le participant correspondant à l'email donné, s'il existe.
		return participantRepository.findByEmail(email);
	}


}
