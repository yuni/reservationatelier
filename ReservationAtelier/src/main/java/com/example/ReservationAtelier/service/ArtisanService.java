package com.example.ReservationAtelier.service;

import com.example.ReservationAtelier.models.Artisan;
import java.util.List;
import java.util.Optional;

public interface ArtisanService {
	Artisan saveArtisan(Artisan artisan);
    Optional<Artisan> getArtisanById(Integer integer);
    List<Artisan> getAllArtisans();
    Artisan updateArtisan(Artisan artisan);
    void deleteArtisan(Integer id);
   

}
