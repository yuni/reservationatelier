package com.example.ReservationAtelier.service;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.ReservationAtelier.config.CustomUserDetails;
import com.example.ReservationAtelier.models.Participant;
import com.example.ReservationAtelier.repository.ParticipantRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	// Référence au repository de Participant pour accéder aux données des participants.
    private final ParticipantRepository participantRepository;

    // Injection de ParticipantRepository à travers le constructeur. PasswordEncoder est injecté mais non utilisé dans cet exemple.
    @Autowired
    public CustomUserDetailsService(ParticipantRepository participantRepository, PasswordEncoder passwordEncoder) {
        this.participantRepository = participantRepository;
    }

    // Méthode pour charger un utilisateur par son email. Lance une exception si l'utilisateur n'est pas trouvé.
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    	// Recherche le participant par email. Lance une UsernameNotFoundException si aucun participant n'est trouvé.
        Participant participant = participantRepository.findByEmail(email)
            .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + email));
        
     // Retourne une instance de CustomUserDetails contenant les informations du participant.
        return new CustomUserDetails(
            participant.getEmail(),     // L'email du participant utilisé comme nom d'utilisateur.
            participant.getMotDePasse(),  // Le mot de passe du participant.
            participant.getId(),   // L'ID du participant
            Collections.emptyList()   // Liste vide des autorités, car cet exemple n'implique pas de rôles spécifiques
        );
    }
}
