package com.example.ReservationAtelier.service;

import  com.example.ReservationAtelier.models.Artisan;
import  com.example.ReservationAtelier.repository.ArtisanRepository;
import jakarta.transaction.Transactional; // Importation pour la gestion des transactions.
import org.springframework.beans.factory.annotation.Autowired; // Importation pour l'injection de dépendance.
import org.springframework.stereotype.Service; // Importation pour indiquer que c'est un service Spring.
import java.util.List;
import java.util.Optional;

@Service // Annoter la classe comme service pour que Spring gère sa création et son cycle de vie.
@Transactional // Annoter la classe pour indiquer que toutes les méthodes publiques sont exécutées
//dans le cadre d'une transaction.
public class ArtisanServiceImpl implements ArtisanService {

    private final ArtisanRepository artisanRepository; // Déclaration du repository pour les artisans.

    @Autowired // Utilisation de l'injection de dépendance pour injecter une instance de ArtisanRepository.
    public ArtisanServiceImpl(ArtisanRepository artisanRepository) {
    	// Affectation du repository injecté au champ de la classe.
        this.artisanRepository = artisanRepository; 
    }

    @Override // Implémentation de la méthode pour enregistrer un atelier.
    public Artisan saveArtisan(Artisan artisan) {
        return artisanRepository.save(artisan); // Appel du repository pour enregistrer le artisan.
    }

    @Override
    public Optional<Artisan> getArtisanById(Integer id) {
        return artisanRepository.findById(id);
              
    }




    @Override // Implémentation de la méthode pour obtenir tous les artisans.
    public List<Artisan> getAllArtisans() {
        return artisanRepository.findAll(); // Appel du repository pour obtenir tous les artisans.
    }

    @Override // Implémentation de la méthode pour mettre à jour un artisan.
    public Artisan updateArtisan(Artisan artisan) {
        return artisanRepository.save(artisan); // Appel du repository pour enregistrer (et donc mettre à jour) l'artisan.
    }

    @Override // Implémentation de la méthode pour supprimer un artisan par son ID.
    public void deleteArtisan(Integer id) {
    	artisanRepository.deleteById(id); // Appel du repository pour supprimer l'artisan.
    }


}
