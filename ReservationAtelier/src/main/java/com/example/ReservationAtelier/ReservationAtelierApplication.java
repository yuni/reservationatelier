package com.example.ReservationAtelier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.ReservationAtelier")
public class ReservationAtelierApplication extends SpringBootServletInitializer{

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReservationAtelierApplication.class);
    }
	
	public static void main(String[] args) {
	// 	exécuter l'application Spring Boot, en démarrant le contexte Spring et en lançant le service web.
		SpringApplication.run(ReservationAtelierApplication.class, args);
	}

}
